$(document).ready(function() {
    var $formWrapper = $('#contact-form-wrapper');

    var $email = $('#contact-form input[name=email]');
    $email.on('blur', validateEmail);
    $email.on('keyup', validateEmail);

    function validateEmail() {
        //console.log('email '+emailValid);

        if ($email.val()) {
            var emailCheck = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,6})$/;
            if (!emailCheck.test($email.val())){

                $formWrapper.find('div.alert').detach();
                $formWrapper.find('.btn-primary').attr('disabled',true);
                errorMessage($email, 'Wrong email format');
            } else {
                $formWrapper.find('div.alert').detach();
                $formWrapper.find('.btn-primary').attr('disabled',false);
            }
        } else {
            $formWrapper.find('div.alert').detach();
            $formWrapper.find('.btn-primary').attr('disabled',true);
            errorMessage($email, 'Email field is required');
        }
    }

    var $name = $('#contact-form input[name=yourname]');
    var $company = $('#contact-form input[name=company]');

    $name.on('change',validateNameCompany);
    $company.on('change',validateNameCompany);

    function validateNameCompany(){
        if (!$name.val()) {
            $formWrapper.find('div.alert').detach();
            $formWrapper.find('.btn-primary').attr('disabled',true);
            errorMessage($name,'Name is required');

        } else $formWrapper.find('div.alert').detach();

        if (!$company.val()) {
            $formWrapper.find('div.alert').detach();
            $formWrapper.find('.btn-primary').attr('disabled',true);
            errorMessage($company, 'Company name is required');
        } else $formWrapper.find('div.alert').detach();

        if ($name.val() && $company.val())
            $formWrapper.find('.btn-primary').attr('disabled',false);
    }
    

    $formWrapper.delegate('.btn-primary', 'click', function(e){
        e.preventDefault();

        var userProfile=$(this).closest('form');
        $formWrapper.find('div.alert').detach();

        var dataUpdate = {
            name: userProfile.find('input[name=yourname]').val(),
            company: userProfile.find('input[name=company]').val(),
            email: userProfile.find('input[name=email]').val(),
            demonstration: userProfile.find('input[name=demonstration]').is('checked')
        };

        $.ajax({
            type: "POST",
            timeout: 800,
            url: 'https://app.sparker.be/api/potentialClient/registerPotentialClient',
            data: dataUpdate,
            crossDomain: true,
            success: function(response){

                successMessage($('#contact-form'), response.message);

            }
        }).fail(function(jqXHR, statusText){
            if (jqXHR.responseJSON) {
                errorMessage($("#contact-form"), jqXHR.responseJSON['detail']);
            } else {
                errorMessage($("#contact-form"), "Failed to connect to server. We are unable to process your request. Please try again later.");
            }
        });
    });

    function successMessage(input, message){
        var messageWrapper = document.createElement('div');
        messageWrapper.setAttribute('class','alert alert-success');
        var t = document.createTextNode(message);
        messageWrapper.appendChild(t);

        input.after(messageWrapper);
    }

    function errorMessage(element, message){
        var messageWrapper = document.createElement('div');
        messageWrapper.setAttribute('class','alert alert-warning');
        var t = document.createTextNode(message);
        messageWrapper.appendChild(t);
        element.before(messageWrapper);
    }

});

